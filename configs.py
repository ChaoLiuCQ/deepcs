
def get_config():   
    conf = {
        'workdir': './data/github/',
        'data_params': {
            # training data
            'train_methname': 'train.methname.h5',
            'train_apiseq': 'train.apiseq.h5',
            'train_tokens': 'train.tokens.h5',
            'train_desc': 'train.desc.h5',
            # valid data
            'valid_methname': 'test.methname.h5',
            'valid_apiseq': 'test.apiseq.h5',
            'valid_tokens': 'test.tokens.h5',
            'valid_desc': 'test.desc.h5',
            # test data
            'use_codebase': 'use.rawcode.txt',
            'use_methname': 'use.methname.h5',
            'use_apiseq': 'use.apiseq.h5',
            'use_tokens': 'use.tokens.h5',
            'use_codevecs': 'ori-test_codevecs_npy/use.codevecs',
            'use_rawcode': 'use_rawcode/use.rawcode',
            'use_search': 'use.search.txt',
            'use_code': 'test.code.txt',
            'use_result': 'test_search/use.result',
            'use_combine': 'use.result.txt',
            # 'n_split': 50,
            'n_split': 10,
            'n_top': 10,

            # parameters
            'methname_len': 6,
            'apiseq_len': 30,
            'tokens_len': 50,
            'desc_len': 30,
            'n_words': 10000,
            'vocab_methname': 'vocab.methname.pkl',
            'vocab_apiseq': 'vocab.apiseq.pkl',
            'vocab_tokens': 'vocab.tokens.pkl',
            'vocab_desc': 'vocab.desc.pkl',
        },               
        'training_params': {
            'batch_size': 128,
            'chunk_size': 40000,
            'nb_epoch': 501,
            'validation_split': 0.2,
            'optimizer': 'adam',
            'valid_every': 1001,
            'n_eval': 100,
            'evaluate_all_threshold': {
                'mode': 'all',
                'top1': 0.4,
            },
            'save_every': 10,
            # epoch that the model is reloaded from . If reload=0, then train from scratch
            'reload': 500,
        },

        'model_params': {
            'model_name': 'JointEmbeddingModel',
            'n_embed_dims': 100,
            # number of hidden dimension of code/desc representation
            'n_hidden': 400,
            # recurrent
            # * 2
            'n_lstm_dims': 200,
            # 'word2vec_100_methname.h5',
            'init_embed_weights_methname': None,
            # 'word2vec_100_tokens.h5',
            'init_embed_weights_tokens': None,
            # 'word2vec_100_desc.h5',
            'init_embed_weights_desc': None,
            'margin': 0.05,
            # similarity measure: gesd, cosine, aesd
            'sim_measure': 'cos',
        }        
    }
    return conf




