Introduction
============
Within this archive you find the replication package of [DeepCS](https://github.com/guxd/deep-code-search) for the article "Simplifying Deep-Learning-Based Model for Code Search" by Chao Liu, Xin Xia, David Lo, Ahmed E. Hassan, and Shanping Li for currently under review at ACM Transactions on Software Engineering and Methodology. The aim of this replication package is to allow other researchers to replicate our results with minimal effort, as well as to provide additional results that could not be included in the article directly.

Requirements
============
python install -r Requirements.txt


Data Contents
========
Data download: get data from the [link](https://bitbucket.org/ChaoLiuCQ/codebase) 

data/janalyzer: the code parsed by our tool [Janalyzer](https://github.com/liuchaoss/janalyzer) from the [raw project data](https://bitbucket.org/ChaoLiuCQ/codebase).

data/parsed: the parsed data from data/janalyzer by the data_parse() script as the DeepCS inputs.

data/encoded and data/test: the encoded data from data/parsed by the data_encode() script.

data/search: the generated data by DeepCS when performing code search on testing data.

data/github: model related data.


Scripts
========

models.py: the defined deep learning model.

codesearcher.py: using models.py for code search.

data_parse.py and data_encode.py for preprocessing testing data.

util.py: some assistant functions.

main.py: performing model training and testing.

config.py: model related configurations.


How to use
==========
Data download: https://pan.baidu.com/s/1faFHRvikd1XAxumUT-4lnA with permanent key(0cgs)

Model training: python main.py --mode train

Model testing: python main.py --mode test

