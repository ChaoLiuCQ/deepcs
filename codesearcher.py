from __future__ import print_function

import pickle
import random
import os
import logging
import tables

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format="%(asctime)s: %(name)s: %(levelname)s: %(message)s")


class CodeSearcher:
    def __init__(self, conf=None):
        self.conf = dict() if conf is None else conf
        self.path = self.conf.get('workdir', '../data/github/codesearch/')
        self.train_params = conf.get('training_params', dict())
        self.data_params = conf.get('data_params', dict())
        self.model_params = conf.get('model_params', dict())

        self.vocab_methname = self.load_pickle(self.path + self.data_params['vocab_methname'])
        self.vocab_apiseq = self.load_pickle(self.path + self.data_params['vocab_apiseq'])
        self.vocab_tokens = self.load_pickle(self.path + self.data_params['vocab_tokens'])
        self.vocab_desc = self.load_pickle(self.path + self.data_params['vocab_desc'])

        self._eval_sets = None

        self._code_reprs = None
        self._code_base = None
        self._code_base_chunksize = 2000000

    def load_pickle(self, filename):
        return pickle.load(open(filename, 'rb'))

    # #### Converting / reverting #####
    def convert(self, vocab, words):
        """convert words into indices"""
        if type(words) == str:
            words = words.strip().lower().split(' ')
        return [vocab.get(w, 0) for w in words]

    def revert(self, vocab, indices):
        """revert indices into words"""
        ivocab = dict((v, k) for k, v in vocab.items())
        return [ivocab.get(i, 'UNK') for i in indices]

    # #### Padding #####
    def pad(self, data, len=None):
        from keras.preprocessing.sequence import pad_sequences
        return pad_sequences(data, maxlen=len, padding='post', truncating='post', value=0)

    def postproc(self, codes_sims):
        codes_, sims_ = zip(*codes_sims)
        codes = [code for code in codes_]
        sims = [sim for sim in sims_]
        final_codes = []
        final_sims = []
        n = len(codes_sims)
        for i in range(n):
            is_dup = False
            for j in range(i):
                if codes[i][:80] == codes[j][:80] and abs(sims[i] - sims[j]) < 0.01:
                    is_dup = True
            if not is_dup:
                final_codes.append(codes[i])
                final_sims.append(sims[i])
        return zip(final_codes, final_sims)

    def load_hdf5(self, vecfile, start_offset, chunk_size):
        """reads training sentences(list of int array) from a hdf5 file"""
        table = tables.open_file(vecfile)
        data, index = (table.get_node('/phrases'), table.get_node('/indices'))
        data_len = index.shape[0]
        # if chunk_size is set to -1, then, load all data
        if chunk_size == -1:
            chunk_size = data_len
        start_offset = start_offset % data_len
        offset = start_offset
        logger.debug("{} entries".format(data_len))
        logger.debug("starting from offset {} to {}".format(start_offset, start_offset + chunk_size))
        sents = []
        while offset < start_offset + chunk_size:
            if offset >= data_len:
                logger.warn('Warning: offset exceeds data length, starting from index 0..')
                chunk_size = start_offset + chunk_size - data_len
                start_offset = 0
                offset = 0
            len, pos = index[offset]['length'], index[offset]['pos']
            offset += 1
            sents.append(data[pos:pos + len].astype('int32'))
        table.close()
        return sents

    def save_model_epoch(self, model, epoch):
        if not os.path.exists(self.path + 'models/' + self.model_params['model_name'] + '/'):
            os.makedirs(self.path + 'models/' + self.model_params['model_name'] + '/')
        model.save("{}models/{}/epo{:d}_code.h5".format(self.path, self.model_params['model_name'], epoch),
                   "{}models/{}/epo{:d}_desc.h5".format(self.path, self.model_params['model_name'], epoch),
                   overwrite=True)

    def load_model_epoch(self, model, epoch):
        assert os.path.exists(
            "{}models/{}/epo{:d}_code.h5".format(self.path, self.model_params['model_name'], epoch)), \
            "Weights at epoch {:d} not found".format(epoch)
        assert os.path.exists(
            "{}models/{}/epo{:d}_desc.h5".format(self.path, self.model_params['model_name'], epoch)), \
            "Weights at epoch {:d} not found".format(epoch)
        model.load("{}models/{}/epo{:d}_code.h5".format(self.path, self.model_params['model_name'], epoch),
                   "{}models/{}/epo{:d}_desc.h5".format(self.path, self.model_params['model_name'], epoch))

    def load_training_data_chunk(self, offset, chunk_size):
        logger.debug('Loading a chunk of training data..')
        logger.debug('methname')
        chunk_methnames = self.load_hdf5(self.path + self.data_params['train_methname'], offset, chunk_size)
        logger.debug('apiseq')
        chunk_apiseqs = self.load_hdf5(self.path + self.data_params['train_apiseq'], offset, chunk_size)
        logger.debug('tokens')
        chunk_tokens = self.load_hdf5(self.path + self.data_params['train_tokens'], offset, chunk_size)
        logger.debug('desc')
        chunk_descs = self.load_hdf5(self.path + self.data_params['train_desc'], offset, chunk_size)
        return chunk_methnames, chunk_apiseqs, chunk_tokens, chunk_descs

    # #### Training #####
    def train(self, model):
        if self.train_params['reload'] > 0:
            self.load_model_epoch(model, self.train_params['reload'])
        save_every = self.train_params.get('save_every', None)
        batch_size = self.train_params.get('batch_size', 128)
        nb_epoch = int(self.train_params.get('nb_epoch', 10))

        nb_start = int(self.train_params['reload']) + 1
        for idx in range(nb_start, nb_epoch):
            print('Epoch %d :: \n' % idx, end='')
            logger.debug('loading data chunk..')
            chunk_methnames, chunk_apiseqs, chunk_tokens, chunk_descs = \
                self.load_training_data_chunk((idx - 1) * self.train_params.get('chunk_size', 100000),
                                              self.train_params.get('chunk_size', 100000))
            logger.debug('padding data..')
            chunk_padded_methnames = self.pad(chunk_methnames, self.data_params['methname_len'])
            chunk_padded_apiseqs = self.pad(chunk_apiseqs, self.data_params['apiseq_len'])
            chunk_padded_tokens = self.pad(chunk_tokens, self.data_params['tokens_len'])
            chunk_padded_good_descs = self.pad(chunk_descs, self.data_params['desc_len'])
            chunk_bad_descs = [desc for desc in chunk_descs]
            random.shuffle(chunk_bad_descs)
            chunk_padded_bad_descs = self.pad(chunk_bad_descs, self.data_params['desc_len'])

            model.fit([chunk_padded_methnames, chunk_padded_apiseqs, chunk_padded_tokens,
                       chunk_padded_good_descs, chunk_padded_bad_descs],
                      epochs=1, batch_size=batch_size)

            if save_every is not None and idx % save_every == 0:
                self.save_model_epoch(model, idx)
