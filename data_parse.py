import util
import multiprocessing
import numpy as np
import os

files_from = 0
files_to = 41026

path_from = 'data/janalyzer/'
path_to = 'data/parsed/'
n_threads = 100


def parse_methname(idx):
    file = path_from + 'file_' + str(idx) + '_Method.csv'
    if os.path.exists(file):
        with open(file, 'r', encoding='utf-8') as infile:
            lines = infile.readlines()

            data = list()
            for i in range(len(lines)):
                line = str(lines[i])
                line = line[line.rindex(',') + 1:]
                line = util.camel_split(line)
                line = util.filter_digit_english(line)
                line = util.get_tokens(line)
                data.append(line)

            util.save_pkl(path_to + 'methname/methname' + str(idx) + '.pkl', data)
        print('over' + str(idx))


def parse_tokens(idx):
    file = path_from + 'file_' + str(idx) + '_Source.csv'
    if os.path.exists(file):
        with open(file, 'r', encoding='utf-8') as infile:
            code = infile.readlines()

            data = list()
            for i in range(len(code)):
                line = str(code[i])
                line = line[line.index(';') + 1:]
                line = util.camel_split(line)
                line = util.filter_digit_english(line)
                token = util.get_tokens(line)
                token = util.remove_keywords(token)
                token = util.remove_stopwords(token)
                token = np.unique(token).tolist()
                data.append(token)

            util.save_pkl(path_to + 'tokens/tokens' + str(idx) + '.pkl', data)
        print('over' + str(idx))


def parse_apiseq(idx):
    file = path_from + 'file_' + str(idx) + '_ParsedCode.csv'
    if os.path.exists(file):
        with open(file, 'r', encoding='utf-8') as infile:
            code = infile.readlines()

            data = list()
            for i in range(len(code)):
                line = str(code[i])
                line = line[line.index(';') + 1:-1]
                name = list()
                if ';' in line:
                    line = line.split(';')
                    for j in range(len(line)):
                        l = line[j].split(',')
                        if len(l) == 3:
                            if l[2] == '':
                                continue
                            if l[0] == 'MethodCallExpr':
                                name.append(l[2][:-2])
                            elif l[0] == 'ObjectCreationExpr':
                                name.append(l[2])
                data.append(name)

            util.save_pkl(path_to + 'apiseq/apiseq' + str(idx) + '.pkl', data)
        print('over' + str(idx))


def multi_apiseq():
    pool = multiprocessing.Pool(processes=n_threads)
    for i in range(files_from, files_to):
        print('parsed: ' + str(i))
        pool.apply_async(parse_apiseq(i, ))
    pool.close()
    pool.join()


def multi_methname():
    pool = multiprocessing.Pool(processes=n_threads)
    for i in range(files_from, files_to):
        print('method: ' + str(i))
        pool.apply_async(parse_methname(i, ))
    pool.close()
    pool.join()


def multi_tokens():
    pool = multiprocessing.Pool(processes=n_threads)
    for i in range(files_from, files_to):
        print('tokens: ' + str(i))
        pool.apply_async(parse_tokens, (i,))
    print('over')
    pool.close()
    pool.join()

if __name__ == '__main__':
    multi_apiseq()
    multi_methname()
    multi_tokens()

