import argparse
import time
import configs
import logging

from models import JointEmbeddingModel
from codesearcher import CodeSearcher

import numpy as np

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format="%(asctime)s: %(name)s: %(levelname)s: %(message)s")


def parse_args():
    parser = argparse.ArgumentParser("Train and Test Code Search(Embedding) Model")
    parser.add_argument("--proto", choices=["get_config"], default="get_config",
                        help="Prototype config to use for config")
    parser.add_argument("--mode", choices=["train", "test"], default='test',
                        help="The mode to run. The `train` mode trains a model;"
                             " the `test` mode evaluate models in a test set.")
    parser.add_argument("--verbose", action="store_true", default=True, help="Be verbose")
    return parser.parse_args()


if __name__ == '__main__':
    print()
    now = time.time()
    args = parse_args()
    conf = getattr(configs, args.proto)()
    codesearcher = CodeSearcher(conf)

    # Define model ######
    logger.info('Build Model')
    model = JointEmbeddingModel(conf)
    model.build()
    optimizer = conf.get('training_params', dict()).get('optimizer', 'adam')
    model.compile(optimizer=optimizer)

    if args.mode == 'train':
        codesearcher.train(model)

    elif args.mode == 'test':

        codesearcher.load_model_epoch(model, codesearcher.train_params['reload'])

        path_dir = 'data/search/'
        n_split = codesearcher.data_params['n_split']
        for i in range(n_split):
            print('repr_code: ' + str(i) + '-' + str(n_split))
            methnames = np.load(path_dir + 'use_methname/use.methname_' + str(i) + '.npy')
            apiseqs = np.load(path_dir + 'use_apiseq/use.apiseq_' + str(i) + '.npy')
            tokens = np.load(path_dir + 'use_tokens/use.tokens_' + str(i) + '.npy')

            padded_methnames = codesearcher.pad(methnames, codesearcher.data_params['methname_len'])
            padded_apiseqs = codesearcher.pad(apiseqs, codesearcher.data_params['apiseq_len'])
            padded_tokens = codesearcher.pad(tokens, codesearcher.data_params['tokens_len'])

            vecs = model.repr_code([padded_methnames, padded_apiseqs, padded_tokens], batch_size=1000)
            np.save(path_dir + 'test_codevecs_npy/use.codevecs_' + str(i) + '.npy', vecs)

        search = open(codesearcher.path + codesearcher.data_params['use_search'], 'r', encoding='utf-8').readlines()

        n_top = codesearcher.data_params['n_top']
        n_hidden = codesearcher.model_params['n_hidden']

        # query representation
        mat_query = np.empty([len(search), n_hidden])
        for i in range(len(search)):
            print('query representation: ' + str(i))
            query = search[i]
            desc = [codesearcher.convert(codesearcher.vocab_desc, query)]
            padded_desc = codesearcher.pad(desc, codesearcher.data_params['desc_len'])
            desc_repr = np.array(model.repr_desc([padded_desc])[0]).tolist()
            mat_query[i] = desc_repr

        for index in range(n_split):
            print('search: ' + str(index))
            code_base = open(path_dir + 'use_rawcode/use.rawcode_' + str(index) + '.txt', 'rb').readlines()
            mat_code = np.load(path_dir + 'test_codevecs_npy/use.codevecs_' + str(index) + '.npy')

            # cosine similarity between queries and code chunk
            dotted = np.dot(mat_query, np.transpose(mat_code))
            norm1 = np.linalg.norm(mat_query, axis=1)
            norm2 = np.linalg.norm(mat_code, axis=1)
            matrix_vector_norms = np.empty([len(norm1), len(norm2)])
            for i in range(0, len(search)):
                matrix_vector_norms[i] = norm1[i] * norm2
            mat_sims = np.divide(dotted, matrix_vector_norms)

            for i in range(0, len(search)):
                print('search: ' + str(index) + '-' + str(n_split) + ',' + str(
                    i) + '-' + str(len(search)))
                vecs_sims = mat_sims[i]
                vecs_neg_sims = np.negative(vecs_sims)
                maxinds = np.transpose(np.argsort(vecs_neg_sims))
                inds = maxinds[0:n_top]
                sims = np.transpose(vecs_sims)
                vec_code = [code_base[int(j)] for j in inds]
                vec_sims = [str(vecs_sims[int(j)]) + '\n' for j in inds]
                file_code = open(path_dir + 'test_search_npy/use.result.code_' + str(i) + '.txt', 'ab')
                file_code.writelines(vec_code)
                file_code.close()
                file_vec = open(path_dir + 'test_search_npy/use.result.vec_' + str(i) + '.txt', 'a',
                                encoding='utf-8')
                file_vec.writelines(vec_sims)
                file_vec.close()

        # combine:
        file_query = open(codesearcher.path + codesearcher.data_params['use_search'], 'rb').readlines()
        n_top = codesearcher.data_params['n_top']

        for i in range(len(file_query)):
            print('combine search: ' + str(i))
            query = file_query[i]
            sims = list(
                map(float, open(path_dir + 'test_search_npy/use.result.vec_' + str(i) + '.txt', 'rb').readlines()))
            code = open(path_dir + 'test_search_npy/use.result.code_' + str(i) + '.txt', 'rb').readlines()

            zipped = zip(code, sims)
            zipped = sorted(zipped, reverse=True, key=lambda x: x[1])
            zipped = codesearcher.postproc(zipped)
            zipped = list(zipped)[:n_top]

            file_result = open(path_dir + 'use.result.txt', 'ab')
            file_result.write(query)
            file_result.close()

            file_result = open(path_dir + 'use.result.txt', 'a')
            search_list = []
            for z in zipped:
                line = str(z[1]) + str(z[0]) + '\r\n'
                search_list.append(line)
            file_result.writelines(search_list)
            file_result.write('\r\n')
            file_result.close()

    later = time.time()
    diff = later - now
    print(diff)
