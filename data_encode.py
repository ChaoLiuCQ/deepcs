import util
import os
import numpy as np

path_from = 'data/parsed/'
path_to = 'data/encoded/'
path_combine_to = 'data/test/'
total_files = 41025


def encoding():
    vocab_apiseq = util.load_pkl('data/github/vocab.apiseq.pkl')
    vocab_methname = util.load_pkl('data/github/vocab.methname.pkl')
    vocab_tokens = util.load_pkl('data/github/vocab.tokens.pkl')

    new_lines = [0, 0, 0, 0]

    for i in range(total_files):
        print(str(i) + '-' + str(total_files)
              + ' -' + str(new_lines[0])
              + '-' + str(new_lines[1])
              + '-' + str(new_lines[2])
              + '-' + str(new_lines[3]))

        file = path_from + 'apiseq/apiseq' + str(i) + '.pkl'
        if not os.path.exists(file):
            continue

        data_apiseq = util.load_pkl(path_from + 'apiseq/apiseq' + str(i) + '.pkl')
        data_methname = util.load_pkl(path_from + 'methname/methname' + str(i) + '.pkl')
        data_tokens = util.load_pkl(path_from + 'tokens/tokens' + str(i) + '.pkl')

        mtx_apiseq = list()
        mtx_methname = list()
        mtx_tokens = list()

        for j in range(len(data_apiseq)):
            data, isnew1 = vocab_get(data_apiseq[j], vocab_apiseq)
            new_lines[0] += isnew1
            mtx_apiseq.append(data)

            data, isnew2 = vocab_get(data_methname[j], vocab_methname)
            new_lines[1] += isnew2
            mtx_methname.append(data)

            data, isnew3 = vocab_get(data_tokens[j], vocab_tokens)
            new_lines[2] += isnew3
            mtx_tokens.append(data)

            new_lines[3] += isnew1 and isnew2 and isnew3

        util.save_pkl(path_to + 'apiseq/apiseq' + str(i) + '.pkl', mtx_apiseq)
        util.save_pkl(path_to + 'methname/methname' + str(i) + '.pkl', mtx_methname)
        util.save_pkl(path_to + 'tokens/tokens' + str(i) + '.pkl', mtx_tokens)
    util.save_pkl(path_to + 'new_lines.pkl', new_lines)


def vocab_get(line, vocab):
    isnew = 0
    data = list()
    for word in line:
        if word in vocab.keys():
            data.append(vocab[word])
    if len(data) == 0:
        isnew = 1
    return data, isnew


def combine_apiseq():
    data = list()
    count = 0
    index = 0
    for i in range(total_files):
        print('apseq_no: ' + str(i) + '-' + str(total_files))
        file = path_to + 'apiseq/apiseq' + str(i) + '.pkl'
        if os.path.exists(file):
            data.extend(util.load_pkl(file))
            count += 1
            if count == 4000:
                np.save(path_combine_to + 'use_apiseq/use.apiseq_' + str(index) + '.npy', np.array(data))
                index += 1
                count = 0
                data = list()
    np.save(path_combine_to + 'use_apiseq/use.apiseq_' + str(index) + '.npy', np.array(data))


def combine_methname():
    data = list()
    count = 0
    index = 0
    for i in range(total_files):
        print('methname_no: ' + str(i) + '-' + str(total_files))
        file = path_to + 'methname/methname' + str(i) + '.pkl'
        if os.path.exists(file):
            data.extend(util.load_pkl(file))
            count += 1
            if count == 4000:
                np.save(path_combine_to + 'use_methname/use.methname_' + str(index) + '.npy', np.array(data))
                index += 1
                count = 0
                data = list()
    np.save(path_combine_to + 'use_methname/use.methname_' + str(index) + '.npy', np.array(data))


def combine_tokens():
    data = list()
    count = 0
    index = 0
    for i in range(total_files):
        print('tokens_no: ' + str(i) + '-' + str(total_files))
        file = path_to + 'tokens/tokens' + str(i) + '.pkl'
        if os.path.exists(file):
            data.extend(util.load_pkl(file))
            count += 1
            if count == 4000:
                np.save(path_combine_to + 'use_tokens/use.tokens_' + str(index) + '.npy', np.array(data))
                index += 1
                count = 0
                data = list()
    np.save(path_combine_to + 'use_tokens/use.tokens_' + str(index) + '.npy', np.array(data))


if __name__ == '__main__':
    encoding()

    combine_apiseq()
    combine_methname()
    combine_tokens()
